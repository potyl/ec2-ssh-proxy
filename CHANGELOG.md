# Changelog

v0.0.11

- Add a GitLab CI pipeline
- Release done through the GitLab CI pipeline

v0.0.10

- Compiled with go 1.18
- Generate ARM64 binaries for Linux and macOS

v0.0.9

- Region can now be taken from the shared config (~/.aws/config)

v0.0.8

- Fix a link in the release page for the windows binary

v0.0.7

- Windows binary is now named `ec2-ssh-proxy.exe`

v0.0.6

- Fix a typo in an error message
- Rename the files in the download section to `ec2-ssh-proxy`

v0.0.5

- Compiled with go 1.14

v0.0.4

- Add --profile for changing the AWS profile

v0.0.3

- Accept --prefix / --suffix when printing the host tag names
- Add --region for changing the AWS region

v0.0.2

- Add --auto-proxy
- Automated release process

v0.0.1

- First release
