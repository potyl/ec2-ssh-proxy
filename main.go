package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
)

// Version of the application
var version = "devel"

// transferStreams transfers the incoming dat of inStream to the socket and the output data received from
// the socket to outStream. This is a minimal implementation of netcat in go.
func transferStreams(connection net.Conn, inStream *os.File, outStream *os.File, address string) {
	log.Printf("transfer: starting socket data transfer with %s\n", address)

	// Human readable labels for the streams
	ioName := make(map[interface{}]string)
	ioName[connection] = address
	ioName[inStream] = "input"
	ioName[outStream] = "output"

	var waitGroup sync.WaitGroup

	// Copy the bytes from the given reader to the given writer.
	ioCopy := func(writer io.Writer, reader io.Reader) {
		defer waitGroup.Done()
		bytes, err := io.Copy(writer, reader)
		if err != nil {
			log.Printf("transfer: failed to copy data from %s -> %s; error: %s\n", ioName[reader], ioName[writer], err)
			return
		}
		log.Printf("transfer: copied %s from %s -> %s\n", bytesCountAsText(bytes), ioName[reader], ioName[writer])
	}

	// Send input to the remote server
	waitGroup.Add(1)
	go ioCopy(connection, inStream)

	// Read all the output that the remove server sends
	waitGroup.Add(1)
	go ioCopy(outStream, connection)

	waitGroup.Wait()
	log.Printf("transfer: end of network transfer\n")
}

// proxyConnect establishes a connection to `address` through an HTTP proxy by requesting that the remote
// connection be established through the `CONNECT` directive.
// It then consumes the standard HTTP header responses that the proxy would send.
// The HTTP document is left untouched as that's the point where the socket is ready for acting as a
// normal socket as if the connection was done directly to `address`.
func proxyConnect(proxyAddress string, address string) (connection net.Conn, err error) {
	// Use a proxy for connecting to a remote host
	log.Printf("proxy: connecting to proxy %s", proxyAddress)
	connection, err = net.Dial("tcp", proxyAddress)
	if err != nil {
		return
	}

	// End for headers section (both for client and server)
	const httpHeaderDelimiter = "\r\n"

	// Request from the proxy a connection
	message := fmt.Sprintf("CONNECT %s HTTP/1.0\r\n", address)
	log.Printf("proxy: sending connect message: %s", message)
	connection.Write([]byte(message))
	connection.Write([]byte(httpHeaderDelimiter))

	// Read the HTTP response until we get an empty line
	log.Printf("proxy: reading proxy response\n")
	reader := bufio.NewReader(connection)
	for {
		var line string
		line, err = reader.ReadString('\n')
		if err != nil {
			return nil, err
		}

		// Check if the headers are over
		if line == httpHeaderDelimiter {
			log.Printf("proxy: connection to remote end (%s) established by proxy\n", address)
			return
		}
	}
}

// main is the main entry point of the program.
func main() {
	args := parseArguments()

	if args.version {
		fmt.Printf("version: %s\n", version)
		return
	}

	if !args.verbose {
		log.SetOutput(ioutil.Discard)
	}

	// Configure an AWS EC2 Client
	awsConfig := aws.Config{}
	if args.region != "" {
		awsConfig.Region = &args.region
	}

	sessionOptions := session.Options{
		SharedConfigState: session.SharedConfigEnable,
		Config:            awsConfig,
	}

	if args.profile != "" {
		sessionOptions.Profile = args.profile
	}
	awsSession := session.Must(session.NewSessionWithOptions(sessionOptions))
	ec2Client := ec2.New(awsSession)

	// Prepare the hostname to search for in EC2
	hostname := strings.ToLower(args.hostname)
	hostname = strings.TrimPrefix(hostname, args.prefix)
	hostname = strings.TrimSuffix(hostname, args.suffix)

	var instance *ec2.Instance
	var err error
	if hostname == "" {
		err = awsListEc2InstanceTags(ec2Client, args.prefix, args.suffix)
		return
	}

	instance, err = awsFindEc2InstanceByTag(ec2Client, hostname)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
		return
	}

	var connection net.Conn
	awsHost := args.awsHostFunc(instance)
	address := fmt.Sprintf("%s:%s", awsHost, args.port)

	var proxy string
	if args.proxy != "" {
		proxy = args.proxy
	} else if args.autoProxy {
		envVars := [...]string{
			"http_proxy",
			"https_proxy",
			"HTTP_PROXY",
			"HTTPS_PROXY",
		}
		for _, envVar := range envVars {
			proxy = os.Getenv(envVar)
			if proxy != "" {
				break
			}
		}
	}

	if proxy != "" {
		log.Printf("main: requesting connection to %s through proxy %s\n", address, proxy)
		connection, err = proxyConnect(proxy, address)
	} else {
		log.Printf("main: connecting directly to %s\n", address)
		connection, err = net.Dial("tcp", address)
	}
	if err != nil {
		log.Println(err)
		os.Exit(1)
		return
	}
	defer connection.Close()

	transferStreams(connection, os.Stdin, os.Stdout, address)
}

// awsFindEc2InstanceByTag finds an EC2 instance who's tag "name" matches the given hostname.
func awsFindEc2InstanceByTag(ec2Client *ec2.EC2, hostname string) (*ec2.Instance, error) {
	var found *ec2.Instance

	callback := func(result *ec2.DescribeInstancesOutput, lastPage bool) bool {
		for _, reservation := range result.Reservations {
			for _, instance := range reservation.Instances {
				for _, tag := range instance.Tags {
					if *tag.Key == "Name" {
						name := strings.ToLower(*tag.Value)
						if name == hostname {
							found = instance
							return false
						}
					}
				}
			}
		}
		return true
	}

	searchParams := awsFilterRunningInstances()
	err := ec2Client.DescribeInstancesPages(searchParams, callback)

	if found == nil {
		err = fmt.Errorf("EC2 instance with tag name %s not found", hostname)
	}
	return found, err
}

// awsListEc2InstanceTags lists all EC2 instance's tag "name".
func awsListEc2InstanceTags(ec2Client *ec2.EC2, prefix string, suffix string) error {
	// Callback that will print all instances
	callback := func(result *ec2.DescribeInstancesOutput, lastPage bool) bool {
		for _, reservation := range result.Reservations {
		Instance:
			for _, instance := range reservation.Instances {
				for _, tag := range instance.Tags {
					if *tag.Key == "Name" {
						name := strings.ToLower(*tag.Value)
						if name == "" {
							name = "(unnamed)"
						}
						fmt.Printf("%s%s%s\n", prefix, name, suffix)
						break Instance
					}
				}
				fmt.Printf("%s%s%s\n", prefix, "(none)", suffix)
			}
		}
		return true
	}

	searchParams := awsFilterRunningInstances()
	err := ec2Client.DescribeInstancesPages(searchParams, callback)

	return err
}

// awsFilterRunningInstances creates the proper search params needed by ec2Client.DescribeInstancesPages().
func awsFilterRunningInstances() *ec2.DescribeInstancesInput {
	return &ec2.DescribeInstancesInput{
		DryRun:     aws.Bool(false),
		MaxResults: aws.Int64(1000),
		Filters: []*ec2.Filter{
			{
				Name: aws.String("instance-state-name"),
				Values: []*string{
					aws.String("running"),
				},
			},
		},
	}
}

// parsedArgs is used for storing the command line arguments passed by the user.
type parsedArgs struct {
	hostname    string
	port        string
	proxy       string
	autoProxy   bool
	suffix      string
	awsHostFunc func(*ec2.Instance) string
	profile     string
	region      string
	prefix      string
	verbose     bool
	version     bool
}

// parseArguments parses the command line arguments and returns their value in the structure `parsedArgs`.
func parseArguments() parsedArgs {
	prefix := flag.String("prefix", "", "prefix added to the hostame")
	suffix := flag.String("suffix", "", "suffix added to the hostame")
	profile := flag.String("profile", "", "aws profile to use")
	region := flag.String("region", "", "aws region to use")
	proxyFlag := flag.String("proxy", "", "request that the connection be made by the given HTTP proxy")
	autoProxy := flag.Bool("auto-proxy", false, "connection be made by a proxy taken from the environment")
	verbose := flag.Bool("verbose", false, "enable verbose mode (to stderr)")
	version := flag.Bool("version", false, "show version number and exit")

	// Should be mutually exclusive only one of these makes sense
	usePrivateDNS := flag.Bool("private-dns", false, "hostname provided by EC2's private DNS")
	usePrivateIP := flag.Bool("private-ip", false, "hostname provided by EC2's private IP")
	usePublicIP := flag.Bool("public-ip", false, "hostname provided by EC2's public IP")

	flag.Usage = func() {
		fmt.Printf("%s: [OPTION]... host [port]\n", filepath.Base(os.Args[0]))
		flag.PrintDefaults()
		os.Exit(0)
	}
	flag.Parse()

	var awsHostFunc func(*ec2.Instance) string
	if *usePrivateDNS {
		awsHostFunc = func(instance *ec2.Instance) string { return *instance.PrivateDnsName }
	} else if *usePrivateIP {
		awsHostFunc = func(instance *ec2.Instance) string { return *instance.PrivateIpAddress }
	} else if *usePublicIP {
		awsHostFunc = func(instance *ec2.Instance) string { return *instance.PublicIpAddress }
	} else {
		awsHostFunc = func(instance *ec2.Instance) string { return *instance.PublicDnsName }
	}

	proxy := ""
	if *proxyFlag != "" {
		u, err := url.Parse(*proxyFlag)
		if err != nil {
			fmt.Printf("Failed to parse proxy url: %s; %s\n", *proxyFlag, err)
			os.Exit(1)
		}

		// Parse the proxy hostname and port
		var hostname string
		var port string
		if u.Hostname() == "" {
			hostname, port, err = net.SplitHostPort(*proxyFlag)
			if err != nil {
				fmt.Printf("Failed to parse proxy %s; not in the format host:port; %s\n", *proxyFlag, err)
				os.Exit(1)
			}
		} else {
			hostname = u.Hostname()
			port = u.Port()
			if port == "" {
				fmt.Printf("Failed to parse proxy %s; missing port\n", *proxyFlag)
				os.Exit(1)
			}
		}
		proxy = fmt.Sprintf("%s:%s", hostname, port)
	}

	hostname := flag.Arg(0)
	port := flag.Arg(1)
	if port == "" {
		port = "22"
	}

	args := parsedArgs{
		hostname:    strings.ToLower(hostname),
		port:        port,
		proxy:       proxy,
		autoProxy:   *autoProxy,
		prefix:      strings.ToLower(*prefix),
		suffix:      strings.ToLower(*suffix),
		awsHostFunc: awsHostFunc,
		profile:     *profile,
		region:      *region,
		verbose:     *verbose,
		version:     *version,
	}

	return args
}

// bytesCountAsText returns a byte count as human readable text representation (ex: "1.2GB").
func bytesCountAsText(bytesCount int64) string {
	const unitSize = 1024

	if bytesCount < unitSize {
		return fmt.Sprintf("%d bytes", bytesCount)
	}

	div, exp := int64(unitSize), 0
	for n := bytesCount / unitSize; n >= unitSize; n /= unitSize {
		div *= unitSize
		exp++
	}

	return fmt.Sprintf("%.1f%cB", float64(bytesCount)/float64(div), "KMGTPE"[exp])
}
