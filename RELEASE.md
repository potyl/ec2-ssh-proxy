# Release

To release `ec2-ssh-proxy` simply use the provided makefile.

Start by making a release tag:

```bash
git tag v1.2.3
```

Then proceed with the release with:

```bash
make release
```

The release will be done in gitlab and visible in https://gitlab.com/potyl/ec2-ssh-proxy/-/releases.
